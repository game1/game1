﻿
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class ballGenerator : MonoBehaviour {

	public float spawnRateInSeconds;
	public float spaceBetweenBalls;
	public float speed;
	public float secondsToSpawn;
	public int maxBallAmount = 5;
	public float minSecSpawnTime;
	public float maxSecSpawnTime;
	public GameObject ball;
	public GameObject sp1;
	public GameObject sp2;
	public GameObject sp3;
	public GameObject sp4;
	public GameObject sp5;
	public GameObject sp6;
	public GameObject sp7;
	public GameObject sp8;
	public GameObject sp9;
	public GameObject sp10;
	public GameObject prefab1;
	public GameObject prefab2;
	public GameObject prefab3;
	public GameObject prefab4;

	List<GameObject> balls = new List<GameObject>();
	List<GameObject> prefabs = new List<GameObject>();

	public Text score;
	
	int ballAmount = 1;
	Vector2 current;
	bool level2=false;
	bool level3=false;


	// Use this for initialization
	void Start () {
		StartCoroutine ("BallGeneratorHelper");
		prefabs.Add (prefab1);
		prefabs.Add (prefab2);
		prefabs.Add (prefab3);
		prefabs.Add (prefab4);
	}


	// Update is called once per frame
	void Update () {
		//if (balls.Count == maxBallAmount) {
			foreach(GameObject go in balls) {
				go.transform.position = Vector3.MoveTowards (go.transform.position, new Vector3 (20, 15, 0), speed * Time.deltaTime);
			}
	}

	IEnumerator BallGeneratorHelper() {
		for(int i=0; i < maxBallAmount; i++) {
			float randomizedTimer = Random.Range(minSecSpawnTime,maxSecSpawnTime);
			print(randomizedTimer);

			yield return new WaitForSeconds(randomizedTimer);
			int spawnpoint = Random.Range(1, 10);
			int randomPrefab = Random.Range(0,4);
			if(spawnpoint == 1){
				balls.Add((GameObject)Instantiate (prefabs[randomPrefab], sp1.transform.position, transform.rotation));
			} else if(spawnpoint == 2){
				balls.Add((GameObject)Instantiate (prefabs[randomPrefab], sp2.transform.position, transform.rotation));
			} else if(spawnpoint == 3){
				balls.Add((GameObject)Instantiate (prefabs[randomPrefab], sp3.transform.position, transform.rotation));
			} else if(spawnpoint == 4){
				balls.Add((GameObject)Instantiate (prefabs[randomPrefab], sp4.transform.position, transform.rotation));
			} else if(spawnpoint == 5){
				balls.Add((GameObject)Instantiate (prefabs[randomPrefab], sp5.transform.position, transform.rotation));
			} else if(spawnpoint == 6){
				balls.Add((GameObject)Instantiate (prefabs[randomPrefab], sp6.transform.position, transform.rotation));
			} else if(spawnpoint == 7){
				balls.Add((GameObject)Instantiate (prefabs[randomPrefab], sp7.transform.position, transform.rotation));
			} else if(spawnpoint == 8){
				balls.Add((GameObject)Instantiate (prefabs[randomPrefab], sp8.transform.position, transform.rotation));
			} else if(spawnpoint == 9){
				balls.Add((GameObject)Instantiate (prefabs[randomPrefab], sp9.transform.position, transform.rotation));
			} else if(spawnpoint == 10){
				balls.Add((GameObject)Instantiate (prefabs[randomPrefab], sp10.transform.position, transform.rotation));
			}
			StartCoroutine("Hardness");
			print(balls.Count);
		}
	}
	IEnumerator Hardness(){
		if(level2==false && balls.Count >= 25){
			speed = speed+2;
			maxSecSpawnTime = 2;
			level2=true;
		}
		if(level3==false && balls.Count >= 50){
			speed = speed+2;
			maxSecSpawnTime = 1;
			minSecSpawnTime = 0;
			level3=true;
		}
		yield return null;
	}
}
